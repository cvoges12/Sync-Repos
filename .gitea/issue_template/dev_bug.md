---

name: "Dev branch Bug"
about: "Report a bug to be fixed."
title: "[fix]/dev "
ref: "dev"
labels:

- bug
- "help wanted"

---

### Environment (answer all that apply)

Operating System (OS) name and version: 

libc name and version: 

Browser name and version: 

Version of each dependency: 

Hardware: 

**\<list any relevant environment variables, software, or other context here>**

### Expected behavior

**\<describe the expected behavior here>**

### Current behavior

**\<describe the current behavior here>**

### Steps to Reproduce

**\<list the steps to reproduce here>**

1. 
2. 
3. 
...

### Error messages

**\<please provide any error messages in code blocks with a label above>**

**\<message labeling the error>**:
```
<error message>
```

### Unit tests, Integration tests, or System tests

**\<please provide any relevant unit tests below>**

**\<please provide any relevant integration tests below>**

**\<please provide any relevant system tests below>**

### Stack trace (if relevant)

### Logs (if any)

### Possible solution(s)

**\<describe any possible solutions here>**

### If bug is confirmed, are you willing to submit a PR and contribute?

[ ] yes
[ ] no
[ ] maybe
[ ] what's a PR?

### Consider funding it!

<https://www.bountysource.com/>
