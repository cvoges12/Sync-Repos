<h1 align="center">
  <a href="https://gitdab.com/cvoges12/Sync-Repos">
    <img src="docs/images/logo.png" alt="Logo" width="100" height="100">
  </a>
  <br />
  Sync-Repos
</h1>

> **[?]**
> Switch for your own logo

<div align="center">
    <i>A simple Git repository synchronization tool</i>
    <br />
    <a href="https://gitdab.com/cvoges12/Sync-Repos/issues/new">
        Report a Bug
    </a>
    ·
    <a href="https://gitdab.com/cvoges12/Sync-Repos/issues/new?template=feature_request.md">
        Request a Feature
    </a>
    ·
    <a href="https://gitdab.com/cvoges12/Sync-Repos/issues/new?template=question.md">
        Ask a Question
    </a>
<br />

[![Project license](https://img.shields.io/badge/license-AGPL--3.0--only-green?style=flat-square)](LICENSE)

[![Pull Requests welcome](https://img.shields.io/badge/PRs-welcome-ff69b4.svg?style=flat-square)](https://gitdab.com/cvoges12/Sync-Repos/compare)
[![code with love by](https://img.shields.io/badge/%3C%2F%3E%20with%20%E2%99%A5%20by-cvoges12-ff1414.svg?style=flat-square)](https://gitdab.com/cvoges12)

Donations accepted:
<br />
[![Liberapay receiving](https://img.shields.io/liberapay/receives/cvoges12.svg?style=flat-square)](https://liberapay.com/cvoges12)
[![buymeacoffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://buymeacoffee.com/cvoges12)
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/cvoges12)

</div>

## Table of Contents

- [About](#about)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Usage](#usage)
- [Support](#support)
- [Roadmap](#roadmap)
- [Project Help](#project-help)
- [Contributing](#contributing)
- [Acknowledgements](#acknowledgements)
    - [Funding Organizations](#funding-organizations)
    - [Patrons](#patrons)
    - [Software Used](#software-used)
    - [Inspirations](#inspirations)
    - [Authors and Contributors](#authors-and-contributors)
- [Security](#security)
- [License](#license)

## About

> **[?]**
> Provide general information about your project here.
> What problem does it (intend to) solve?
> What is the purpose of your project?
> Why did you undertake it?

**[Back to top](#table-of-contents)**

## Getting Started

### Prerequisites

Required dependencies:
- .

> **[?]** 
> List project requirements or dependencies.
> List build tools.

**[Back to top](#table-of-contents)**

### Installation

Run the following to install:
```sh
```

> **[?]**
> Describe how to install and get started with the project.
> List build instructions.

**[Back to top](#table-of-contents)**

## Usage

> **[?]**
> How does one go about using it?
> Provide various use cases and code examples here.

**[Back to top](#table-of-contents)**

## Support

> **[?]**
> Provide additional ways to contact the project maintainer/maintainers.
> Website?
> Email?

**[Back to top](#table-of-contents)**

## Roadmap

See the [open issues](https://gitdab.com/cvoges12/Sync-Repos/issues) 
for a list of proposed features (and known issues).

For some quick links:
- [Top Feature Requests](https://gitdab.com/cvoges12/Sync-Repos/issues?state=open&sort=mostcomment&labels=)
- [Newest Feature Requests](https://gitdab.com/cvoges12/Sync-Repos/issues?state=open&sort=latest&labels=)
- [Hottest Feature Requests](https://gitdab.com/cvoges12/Sync-Repos/issues?state=open&sort=mostcomment&labels=)
- [Top Bugs](https://gitdab.com/cvoges12/Sync-Repos/issues?state=open&sort=mostcommented&labels=)
- [Newest Bugs](https://gitdab.com/cvoges12/Sync-Repos/issues?state=open&sort=latest&labels=)
- [Hottest Bugs](https://gitdab.com/cvoges12/Sync-Repos/issues?state=open&sort=mostcomment&labels=)

> **[?]**
> Add number to `labels=` in link.
> Add Kanban board or other project management software.

**[Back to top](#table-of-contents)**

## Project Help

If you want to say "thank you" and/or support active development of 
**Sync-Repos**:
- Add a [GitHub Star](https://github.com/cvoges12/Sync-Repos) to the 
project's GitHub awareness page. (*our project is not mirrored to prevent 
GitHub Copilot*)
- Tweet about Sync-Repos or spread the word on other social media.
- Write interesting articles about the project on [Dev.to](https://dev.to/), 
[Medium](https://medium.com/), or your personal blog.
- Become a patron and help fund **Sync-Repos**:
  - [Liberapay](https://liberapay.com/cvoges12)
  - [Buy me a coffee](https://www.buymeacoffee.com/cvoges12)
  - [Ko-Fi](https://ko-fi.com/cvoges12)

> **[?]**
> Replace the above links with links to the project's own funding pages.

> **[?]**
> Optionally, add other donation links, such as:
> Patreon, 
> Kickstarter, 
> Indiegogo

Together, we can make Sync-Repos **better**!

**[Back to top](#table-of-contents)**

## Contributing

Firstly, thanks for considering to contribute! 
Contributions are what make the open-source community such an amazing place to 
learn, inspire, and create. 
Any contributions you make will benefit everybody else and are **greatly 
appreciated**.

Please read [our contribution guidelines](docs/CONTRIBUTING.md), and thank you 
for being involved!

**[Back to top](#table-of-contents)**

## Acknowledgements

### Funding Organizations

> **[?]**
> Include any organizations or companies that are funding the project.

**[Back to top](#table-of-contents)**

### Patrons

A special thanks to my patrons on:
- [Liberapay](https://liberapay.com/cvoges12)
- [Buy me a coffee](https://www.buymeacoffee.com/cvoges12)
- [Ko-Fi](https://ko-fi.com/cvoges12)

> **[?]**
> Replace the above links with links to the project's own funding pages w/ 
> patrons listed.

> **[?]**
> Optionally, add other donation links, such as:
> Patreon, 
> Kickstarter, 
> Indiegogo

**[Back to top](#table-of-contents)**

### Software Used

- The 
[Amazing Github Template](https://github.com/dec0dOS/amazing-github-template) 
for the README.md and some other documents.
- The [cvoges12 app-template](https://gitdab.com/cvoges12/app-template) for this template repository.

> **[?]**
> Include libraries and applications used in the project.

**[Back to top](#table-of-contents)**

### Inspirations

> **[?]**
> Include any projects or people that have inspired this project.

**[Back to top](#table-of-contents)**

### Authors and contributors

The original setup of this repository is by cvoges12.

For a full list of all authors and contributors, see 
[the contributors page](https://gitdab.com/cvoges12/Sync-Repos/activity).

> **[?]**
> Replace with contributors graph once 
> https://github.com/go-gitea/gitea/issues/847
> is implemented.

**[Back to top](#table-of-contents)**

## Security

**Sync-Repos** follows good practices of security, but 100% security cannot 
be assured.
**Sync-Repos** is provided **"as is"** without any **warranty**. Use at your 
own risk.

_For more information and to report security issues, please refer to our 
[security documentation](docs/SECURITY.md)._

**[Back to top](#table-of-contents)**

## License

Copyright (C) 2022 Clayton Voges

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU Affero General Public License as published by the Free 
Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more 
details.

You should have received a copy of the GNU Affero General Public License along 
with this program. If not, see <https://www.gnu.org/licenses/>

**[Back to top](#table-of-contents)**
